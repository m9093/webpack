const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')

module.exports = {
    entry : './src/index.js',
    output : {
        filename : '[name].js',
        path : path.resolve(__dirname , 'dist'),
        clean : true
    },
    mode: 'development',
    devServer : {
        static : {
            directory: path.join(__dirname, 'dist'),
        },
        open : true
    },
    plugins : [new HtmlWebpackPlugin() , new CleanWebpackPlugin],
    watch: true
}