# Webpack

## Khái niệm
   - Webpack là gì : webpack là một thư viện bundles cho các ứng dụng JS hiện đại. Khi sử dụng webpack trong ứng dụng giúp build các dependency từ các entry point thành các file  bundles - là các tài nguyên tĩnh phục vụ trong ứng dụng . Phiên bản hiện tại là webpack 5. sử dụng trên nền nodejs 10.+
   - Webpack dùng làm gì : Khi sử dụng module js chỉ có thể chạy trên môi trường nodeJs , khi lên môi trường browser thì ko thể hiểu được. Webpack cho phép sử dụng module trong khi code thoải mái (có thể sử dụng cả require và import ) và sau đó bundles lại và sử dụng trên trình duyệt .
   optimize các tài nguyên, spliting import , giảm kích thước proj
   [Why we use webpack](https://webpack.js.org/concepts/why-webpack/)
## Module javascript kiểu "old-school"
   - viết các file js rồi import vào file html . thứ tự import trước thì biến , hàm được khởi tạo trước
## CommonJS
   - Việc thêm các thể script để import js vào file .html xảy ra vấn đề về scrope , size . CommonJS ra đời và giới thiệu require giúp gọi giữa các file js với nhau 
## Webpack.config.js
- vị trí config đặt cùng cấp với /src , /dist
- Cấu trúc là một file js export ra một object gồm các thuộc tính : 
   - Entry : chỉ ra module nào webpack bắt đầu build, mặc định sẽ là **./src/index.js**
   - Output : chỉ ra nơi lưu các file bundles và cách đặt tên các file đó , thông thường là **dist/main.js**
   - Loaders : mặc định webpack chỉ có thể hiểu file js , json . Sử dụng Loaders cho phép xử lý các file kiểu khác trước khi compile vào dependencies graph , chỉ sử dụng với module. Trong các loader của webpack nổi bật có babel , một loader giúp biên dịch code javascript từ phiên bản ES cao xuống thấp qua đó cho phép deverlop code dùng ES6 nhưng vẫn chạy được trên các trình duyệt cũ
   - Plugin : plugin cho phép sử dụng với phạm vị cả project , plugin có thể sử dụng từ bên ngoài và cũng có built-in
   - Mode : có các mode là  `development` , `production` , `none` mặc định là `production` , các mode khác nhau cho kết quả build khác nhau
   - Browser Compatibility
```
   module.exports = {
      entry : './src/index.js',
      output : {
         fileName : fileName,
         path : Path
      },
      plugins : [plugin]
   }
```
## CommandLine 
-  Run webpack : `npx webpack`  // nếu có file webpack.config.js thì nó sẽ tự động tìm nạp 
-  Run with config fig : `npx webpack --config <fileConfig>`   
-  Run with specific mode : `npx webpack --mode=<mode>` priority của cmd cao hơn trong file config
-  Watch mode : `npx webpack --watch`